## Модуль Swift_ProductRating
Предоставляет функционал получения подтверждённого рейтинга продуктов.

Из таблиц `rating_option_vote_aggregated` и `review_entity_summary` получается подтверждённый рейтинг
и количество отзывов для всех сторов (`store_id = 0`) и записывается в индекс `swift_product_rating`, содержащий поля:
- ID Продукта - `product_id`
- ID Магазина - `store_id`
- Количество отзывов - `reviews_count`
- Средний рейтинг продукта - `average_rating`

Команда реиндекса - `bin/magento indexer:reindex swift_product_rating`