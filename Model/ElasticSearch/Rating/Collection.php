<?php

namespace Swift\ProductRating\Model\ElasticSearch\Rating;

use Magento\ElasticSearch\ModelElastic\Model\AbstractCollection;

class Collection extends AbstractCollection
{
    public function getIndexName()
    {
        return $this->engine->buildIndexName(Index::IDENTIFIER);
    }
}
