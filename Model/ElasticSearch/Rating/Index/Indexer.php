<?php

namespace Swift\ProductRating\Model\ElasticSearch\Rating\Index;

use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Search\Request\Dimension;
use Magento\CatalogSearch\Model\Indexer\IndexerHandlerFactory;
use Magento\Framework\Indexer\ScopeResolver\IndexScopeResolver;
use Magento\SearchBase\Model\Index\Indexer as SearchBaseIndexer;
use Swift\ProductRating\Model\ElasticSearch\Rating\Index\ProductRatingsProviderFactory;

class Indexer extends SearchBaseIndexer
{
    protected $productRatingsProviderFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        IndexScopeResolver $indexScopeResolver,
        IndexerHandlerFactory $indexHandlerFactory,
        ProductRatingsProviderFactory $productRatingsProviderFactory
    ) {
        $this->productRatingsProviderFactory = $productRatingsProviderFactory;
        parent::__construct(
            $storeManager,
            $indexHandlerFactory,
            $indexScopeResolver
        );
    }

    public function rebuildStoreIndex($storeId, $ids = null, $part = null)
    {
        $productRatingsProvider = $this->productRatingsProviderFactory->create();
        $ratings = $this->index->getSearchableEntities($storeId, $ids);
        return $productRatingsProvider->getProductRatings($ratings);
    }

    public function reindexPartial($ids, $storeId = null, $part = null)
    {
        $configData = [
            'fieldsets' => [],
            'indexer_id' => $this->index->getIndexName(),
            'part' => $part,
        ];
        $dimension = new Dimension('scope', Store::DEFAULT_STORE_ID);
        $indexHandler = $this->indexHandlerFactory->create(['data' => $configData]);

        $productRatingsProvider = $this->productRatingsProviderFactory->create();
        $indexHandler->deleteIndex([$dimension], $productRatingsProvider->getRatingRowIds($ids));
        $indexHandler->saveIndex(
            [$dimension],
            $this->rebuildStoreIndex(Store::DEFAULT_STORE_ID, $ids, $part)
        );

        return true;
    }
}
