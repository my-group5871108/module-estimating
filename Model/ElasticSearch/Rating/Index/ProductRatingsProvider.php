<?php

namespace Swift\ProductRating\Model\ElasticSearch\Rating\Index;

use Swift\ProductRating\Model\ElasticSearch\Rating\Index;
use Swift\ProductRating\Model\ElasticSearch\Rating\CollectionFactory;
use Magento\ElasticSearch\ModelElastic\Model\Collection\BatchProvider;

class ProductRatingsProvider
{
    const DEFAULT_BATCH_SIZE = 1000;

    protected $batchSize;
    protected $batchProvider;
    protected $ratingCollectionFactory;

    public function __construct(
        BatchProvider $batchProvider,
        CollectionFactory $ratingCollectionFactory,
        int $batchSize = self::DEFAULT_BATCH_SIZE
    ) {
        $this->batchProvider = $batchProvider;
        $this->ratingCollectionFactory = $ratingCollectionFactory;
        $this->batchSize = $batchSize;
    }

    public function getProductRatings($ratings)
    {
        foreach ($ratings as $rating) {
            yield from $this->prepareRatingDocument($rating);
        }
    }

    protected function prepareRatingDocument($rating)
    {
        $id = (int)$rating['primary_id'];

        $doc[$id] = [
            'id' => $id,
            'product_id' => (int)$rating['entity_pk_value'],
            'store_id' => $rating['store_id'],
            'reviews_count' => $rating['reviews_count'],
            'average_rating' => isset($rating) ? round($rating['rating_summary'] / 20, 1) : null
        ];

        return $doc;
    }

    public function getRatingRowIds($ratingIds)
    {
        if (!$ratingIds) {
            return;
        }

        $ratingCollection = $this->getRatingCollection($ratingIds);
        $batchIterator = $this->batchProvider->getItems($ratingCollection, $this->batchSize, Index::ROW_ID);

        foreach ($batchIterator as $rowIdsData) {
            yield from $this->prepareRowIds($rowIdsData);
        }
    }

    protected function getRatingCollection($ratingIds)
    {
        return $this->ratingCollectionFactory->create()
            ->addAttributeToSelect([Index::ROW_ID])
            ->addFieldToFilter('id', ['in' => $ratingIds]);
    }

    protected function prepareRowIds($rowIdsData)
    {
        $rowIds = array_column($rowIdsData, Index::ROW_ID);
        return array_combine($rowIds, $rowIds);
    }
}
