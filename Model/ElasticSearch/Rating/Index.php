<?php

namespace Swift\ProductRating\Model\ElasticSearch\Rating;

use Magento\SearchBase\Model\Index\Context;
use Magento\SearchBase\Model\Index\AbstractIndex;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Swift\ProductRating\Model\ResourceModel\Rating as RatingResource;

class Index extends AbstractIndex
{
    const ROW_ID      = 'id';
    const PRODUCT_SKU = 'sku';
    const STORE_ID    = 'store_id';
    const RATING_ID   = 'rating_id';
    const IDENTIFIER  = 'product_rating';

    protected $ratingResource;

    public function __construct(
        Context $context,
        EventManager $eventManager,
        RatingResource $ratingResource,
        $dataMappers = []
    ) {
        $this->ratingResource = $ratingResource;
        parent::__construct(
            $context,
            $dataMappers,
            $eventManager
        );
    }

    public function getName()
    {
        return __('Swift / Product Rating');
    }

    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    public function getIndexName()
    {
        return $this->getIdentifier();
    }

    public function getAttributes()
    {
        return [
            self::RATING_ID => __('Rating Id'),
            self::STORE_ID => __('Store View'),
            self::PRODUCT_SKU => __('Product Sku')
        ];
    }

    public function getSearchableEntities($storeId, $entityIds = null, $lastEntityId = 0, $limit = 100)
    {
        if ($entityIds) {
            return $this->ratingResource->getProductRatingsByIds($entityIds);
        }

        return $this->ratingResource->getProductRatings();
    }

    public function reindexAll($storeId = null, $part = null)
    {
        $result = parent::reindexAll($storeId, $part);
        return $result;
    }

    public function getPrimaryKey()
    {
        return self::ROW_ID;
    }
}
