<?php

namespace Swift\ProductRating\Model\Indexer;

use Psr\Log\LoggerInterface;
use Swift\ProductRating\Model\ElasticSearch\Rating\Index;
use Magento\SearchBase\Api\Repository\IndexRepositoryInterface;
use Magento\Framework\Mview\ActionInterface as MviewActionInterface;
use Magento\Framework\Indexer\ActionInterface as IndexerActionInterface;

class Rating implements IndexerActionInterface, MviewActionInterface
{
    const IDENTIFIER = 'swift_product_rating';

    protected $logger;
    protected $indexRepository;

    public function __construct(
        LoggerInterface $logger,
        IndexRepositoryInterface $indexRepository
    ) {
        $this->logger = $logger;
        $this->indexRepository = $indexRepository;
    }

    public function execute($ids)
    {
        $this->executeList($ids);
    }

    public function executeFull()
    {
        try {
            $this->getIndex()->reindexAll();
        } catch (\Exception $e) {
            $this->logger->info('Error full reindex: ' . $e->getMessage());
        }
    }

    public function executeList(array $ids)
    {
        try {
            $this->getIndex()->reindexPartial($ids);
        } catch (\Exception $e) {
            $this->logger->info('Error partial reindex: ' . $e->getMessage());
        }
    }

    public function executeRow($id)
    {
        $this->executeList([$id]);
    }

    protected function getIndex()
    {
        return $this->indexRepository->getInstance(Index::IDENTIFIER);
    }
}
