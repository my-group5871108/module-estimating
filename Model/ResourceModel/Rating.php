<?php

namespace Swift\ProductRating\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Rating extends AbstractDb
{
    const DEFAULT_STORE_ID     = 0;
    const MINIMUM_RATING_VALUE = 0;

    protected function _construct()
    {
        $this->_init('rating_option_vote_aggregated', 'primary_id');
    }

    public function getProductRatings()
    {
        $query = $this->getQueryProductRatings();
        return $this->getConnection()->fetchAssoc($query);
    }

    public function getProductRatingsByIds($ids)
    {
        $query = $this->getQueryProductRatings()->where('agg.primary_id IN (?)', $ids);
        return $this->getConnection()->fetchAssoc($query);
    }

    protected function getQueryProductRatings()
    {
        $connection = $this->getConnection();
        $tableAggregateRating = $this->getMainTable();
        $tableRating = $connection->getTableName('review_entity_summary');

        $query = $connection->select()->from(
            ['agg' => $tableAggregateRating],
            ['entity_pk_value', 'primary_id', 'percent_approved', 'store_id']
        )
            ->joinLeft(
                ['rat' => $tableRating],
                "rat.entity_pk_value=agg.entity_pk_value",
                ['rating_summary', 'reviews_count']
            )
            ->where('agg.store_id=?', self::DEFAULT_STORE_ID)
            ->where('rat.store_id=?', self::DEFAULT_STORE_ID)
            ->where('agg.percent_approved>?', self::MINIMUM_RATING_VALUE);

        return $query;
    }
}
