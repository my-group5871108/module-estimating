<?php

namespace Swift\ProductRating\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Swift\ProductRating\Model\ElasticSearch\Rating\CollectionFactory as RatingCollectionFactory;

class Rating implements ResolverInterface
{
    protected $ratingCollectionFactory;

    public function __construct(RatingCollectionFactory $ratingCollectionFactory)
    {
        $this->ratingCollectionFactory = $ratingCollectionFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $productItem = null,
        array $args = null
    ): array {
        $productId = $productItem['id'] ?? '';
        return $this->getElasticRatingCollection($productId);
    }

    public function getElasticRatingCollection($productId)
    {
        return $this->ratingCollectionFactory->create()
            ->addAttributeToSelect(['average_rating', 'reviews_count'])
            ->addFieldToFilter('product_id', ['in' => $productId])
            ->getItems();
    }
}
