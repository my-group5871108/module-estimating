<?php

namespace Swift\ProductRating\Setup;

use Swift\Core\Model\Indexer\IndexerProvider;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Swift\ProductRating\Model\Indexer\Rating as IndexerRating;
use Magento\SearchBase\Api\Repository\IndexRepositoryInterface;
use Swift\ProductRating\Model\ElasticSearch\Rating\Index as RatingIndex;

class InstallData implements InstallDataInterface
{
    const POSITION_VALUE = 60;
    const SAMPLE_VALUE   = 1;

    protected $ratingIndex;
    protected $indexRepository;
    protected $indexerProvider;

    public function __construct(
        RatingIndex $ratingIndex,
        IndexRepositoryInterface $indexRepository,
        IndexerProvider $indexerProvider
    ) {
        $this->ratingIndex = $ratingIndex;
        $this->indexRepository = $indexRepository;
        $this->indexerProvider = $indexerProvider;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->addIndexData();
        $setup->endSetup();
    }

    protected function addIndexData()
    {
        $productRatingIndex = $this->indexRepository->get(RatingIndex::IDENTIFIER);

        if (!$productRatingIndex->getId()) {
            $productRatingIndex = $this->indexRepository->create()
                ->setIdentifier($this->ratingIndex->getIdentifier())
                ->setTitle($this->ratingIndex->getName())
                ->setIsUseTmpIndex(true)
                ->setIsActive(true)
                ->setPosition(self::POSITION_VALUE)
                ->setAttributes(array_fill_keys(array_keys($this->ratingIndex->getAttributes()), self::SAMPLE_VALUE));
            $this->indexRepository->save($productRatingIndex);
        }

        $indexer = $this->indexerProvider->getIndexer(IndexerRating::IDENTIFIER);
        $indexer->setScheduled(true);
    }
}
